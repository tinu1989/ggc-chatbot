var IncomingWebhook = require('@slack/client').IncomingWebhook;
var WebClient = require('@slack/client').WebClient;
var webhook = new IncomingWebhook('https://hooks.slack.com/services/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');
var web = new WebClient(global.config.SLACK_TOKEN);
var channel = global.global.config.SLACK_CHANNEL;
const createSlackEventAdapter = require('@slack/events-api').createSlackEventAdapter;
const slackEvents = createSlackEventAdapter('WkBpCXxsOcJnoc2DZPQjNt7g');
slackEvents.on('message', (event)=> {
  console.log(`Received a message event: user ${event.user} in channel ${event.channel} says ${event.text}`);
});
module.exports = function () {
    var slackWebApi ={
        send:send,
		interruptsend:interruptsend
    }  
    return slackWebApi;
    // function send(msg){
    //     webhook.send(msg, function(err, res) {
    //         if (err) {
    //         console.log('Error:', err);
    //         } else {
    //         console.log('Message sent: ', res);
    //         }
    //     });      	
	// }    

    function send(msg,channel){
        var formattedMsg = formatMessage(msg);
        web.chat.postMessage(channel,formattedMsg ,function(err, res) {
            if (err) {
          //  console.log('Error:', err);
            } else {
         //   console.log('Message sent: ', res);
            }
        });      
    }
	
    function interruptsend(msg,channel){
      console.log("in itnmerrrupt");
        web.chat.postMessage(channel,msg ,function(err, res) {
          if (err) {
            console.log('Error:', err);
          }else{
            console.log('Message sent: ', res);
          }
        });      
    }

  function formatMessage(messageObject){
      var formattedMessage;
      for(var key in messageObject){
         formattedMessage = formattedMessage ? formattedMessage+ "*"+key+"* :"+messageObject[key]+"\n" : "*"+key+"* :"+messageObject[key]+"\n"
      }
    return formattedMessage;
  }

}