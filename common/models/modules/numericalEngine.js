const algolia = require("./algolia")();
var deepcopy = require("deepcopy");
var Q = require('q');
module.exports = function (vehicleEngine) {
  var searchEngine = {
    findCar: findCar
  }
  return searchEngine;
  function findCar(apiResponse, callBack) {
    //gogocar_price
    algolia.getNumericalFromAlgolia(apiResponse.result.parameters.NumericSearchParameters)
      .then(function (data) {
        if (data && data.length) {
          var payload = { "type": "card", "results": data };
          apiResponse.result.fulfillment.payload = payload;
        } else {
          apiResponse.result.fulfillment.speech = 'Sorry!, I don’t have enough information to answer your question. You can get started with these.';
        }
        return callBack('', apiResponse);
      })
      .catch(function (err) {
        console.log(err);
      })
  }

};