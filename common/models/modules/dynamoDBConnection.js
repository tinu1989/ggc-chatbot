var AWS=require("aws-sdk");
var Q = require('q');
AWS.config.update({accessKeyId: "AKIAJFBBEW72HORF3WHQ", secretAccessKey: "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"});
AWS.config.region = 'us-east-1';
var dynamodb = new AWS.DynamoDB();
var params = {};
var docClient = new AWS.DynamoDB.DocumentClient();
module.exports = function () {
    var dynamoDBFn = {
        listTables  : dynamodb.listTables,//(params,callback);
        createTable : dynamodb.createTables,//(params,callback);
        insert      : createItem,
        getItem     : readItem,
        removeItem  : deleteItem,
        scanItem	: scanItem,
        updateItem	: updateItem,
		batchWriteItem : batchWriteItem,
		queryGetDelItem : queryGetDelItem
    }
    return dynamoDBFn;
    
    function createItem(data,tableName){
    	var deferred = Q.defer();
	    var params = {
	  		Item: data,
		    ReturnConsumedCapacity: "TOTAL", 
		    TableName: tableName
		 };
		docClient.put(params, function(err, data) {
			console.log(err);
			console.log(data);
		   if (err) {
		   	return deferred.reject(err);
		   }
		   else {
		   	return deferred.resolve({error:0});
		   } 
		});

		return deferred.promise;
	}
	function updateItem(data,Key,exp_attributes,tableName){
    	var deferred = Q.defer();
	    var params = {
	    	Key:Key,
	  		UpdateExpression: data,
		    ExpressionAttributeValues:exp_attributes,
		    ReturnConsumedCapacity: "TOTAL", 
		    TableName: tableName
		 };
		docClient.update(params, function(err, data) {
			console.log(err);
			console.log(data);
		   if (err) {
		   	return deferred.reject(err);
		   }
		   else {
		   	return deferred.resolve({error:0});
		   } 
		});

		return deferred.promise;
	}
	function deleteItem(data,tableName){
		var deferred = Q.defer();
		var params = {
		    TableName: tableName,
		    Key:data
		};
		docClient.delete(params, function(err, data) {
			console.log(data);
		  console.log(err);
		   if (err) {
		   	return deferred.reject(err);
		   }
		   else {
		   	return deferred.resolve({error:0});
		   } 
		});
		return deferred.promise;
	}
	function scanItem(params){
		var deferred = Q.defer();	
		docClient.scan(params, function(err, data) {
			   if (err) {
			   	return deferred.reject(err);
			   }
			   else {
			   	return deferred.resolve(data);
			   } 
		});		
		return deferred.promise;
	}


	function readItem(data,tableName){
		var deferred = Q.defer();
		var params = {
		    TableName: tableName,
		    Key:data
		};
		docClient.get(params, function(err, data) {
			console.log(err);
		   if (err) {
		   	return deferred.reject(err);
		   }
		   else {
		   	console.log(data);
		   	return deferred.resolve({error:0});
		   } 
		});
		return deferred.promise;
	}

	function queryGetDelItem(getparams){
		var DeleteRequestItems = [];
		var deferred = Q.defer();
		dynamodb.query(getparams, function (err, data) {
			if (err) {
				return deferred.reject(err);
			} else {
				data.Items.forEach(function(pushelements){
					var pushItems = 
					{ 
						DeleteRequest: { 
							Key: { 
								"id":  pushelements.id, 
								"time": pushelements.time
							
							} 
						} 
					}
					DeleteRequestItems.push(pushItems);
				});
				return deferred.resolve(DeleteRequestItems);
			}		
		});
		return deferred.promise;
	}

	function batchWriteItem(batchRequest){
		var deferred = Q.defer();
		dynamodb.batchWriteItem(batchRequest,function (err, data) {
			if (err) {
				return deferred.reject(err);
			} else {
				return deferred.resolve("Successfully deleted");
			}	
		});
		return deferred.promise;
	}
	
}

