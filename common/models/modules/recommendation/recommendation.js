const algolia = require("../algolia")();
var Q = require('q');
var unirest = require("unirest");
const jsonObject = {
  "type_name": "Select a Body Type"
}
module.exports = function (Chatagent) {
  var recommendation = {
    getRecommendation: getRecommendation,
    cognubAPI: cognubAPI
  }
  return recommendation;
  function getRecommendation(apiResponse, m_callback) {
    var InventorySearchParameters;
    var searchObj = {}, finishFlag = 1;
    for (var key in apiResponse.result.parameters) {
      if (!apiResponse.result.parameters[key]) {
        InventorySearchParameters = key;
        finishFlag = 0;
        break;
      }
      else
        searchObj[key] = apiResponse.result.parameters[key];
    }


    if (!finishFlag)
      searchObj["InventorySearchParameters"] = [InventorySearchParameters];
    switch (finishFlag) {
      case 0:
        algolia.algoliaSearch(searchObj)
          .then(function (data) {
            if (data.nbHits > 0) {
              apiResponse.result.fulfillment.speech = jsonObject[InventorySearchParameters];
              apiResponse.result.fulfillment.payload = {'type':'button','results' : []};
              apiResponse.result.fulfillment.payload.results = data["factes"][InventorySearchParameters].data;
            }
            Chatagent.app.models.chatHistory.addchathistory(apiResponse, 'bot', 'incoming');
            return m_callback('', apiResponse);
          })
        break;
      case 1:
        var algoliaReq = [], algoliaRes = [];
        recommendation.cognubAPI(apiResponse.result.parameters.type_name)
          .then(function (RecommendationData) {
            for (var i = 0; i < RecommendationData.length; i++) {
              var searchObj = {
                "model_name": RecommendationData[i].model,
                "type_name": RecommendationData[i].carType,
                "InventorySearchParameters": ["make_name", "model_name", "vin", "gogocar_price", 'dataone_image_path', 'model_year']
              }
              if (i == 10)
                break;
              algoliaReq.push(algolia.algoliaSearch(searchObj));
            }
            var promise = Q.all(algoliaReq);
            promise.then(function (algoliaResponse) {
              for (var i = 0; i < algoliaResponse.length; i++) {
                var test = algoliaResponse[i] != null ? algoliaResponse[i] : {};
                if (algoliaResponse[i] && algoliaResponse[i].nbHits) {
                  if (!algoliaResponse[i].hits[0].dataone_image_path)
                    delete algoliaResponse[i].hits[0].dataone_image_path;
                  algoliaResponse[i].hits[0]['subtype'] = 'card';
                  delete algoliaResponse[i].hits[0]._highlightResult;
                  delete algoliaResponse[i].hits[0]._rankingInfo;
                  algoliaRes.push(algoliaResponse[i].hits[0]);
                }

              }
              if (algoliaRes.length == 0)
                apiResponse.result.fulfillment.speech = 'Sorry! We can\'t find your Recommended car';
              else {
                apiResponse.result.fulfillment.speech = "These are the recommended cars"
                apiResponse.result.fulfillment.payload = {'type':'card','results':[]}
                apiResponse.result.fulfillment.payload.results = algoliaRes
              }
              Chatagent.app.models.chatHistory.addchathistory(apiResponse, 'bot', 'incoming');
              return m_callback('', apiResponse);
            })
          })
          .catch(function (err) {
            apiResponse.result.fulfillment.speech = 'Sorry! We can\'t find any Recommendation';
            return m_callback('', apiResponse);
          })

    }
  }

  function cognubAPI(type_name) {
    var deferred = Q.defer();
    unirest.post(global.config.RECOMMENDATION)
      .set('Content-Type', 'application/json')
      .send({ "data": { Cartype: type_name } })
      .end(function (response) {
        if (!response.body.TopScoring) {
          return deferred.reject(response.body);
        }
        return deferred.resolve(response.body.TopScoring);
      })
    return deferred.promise;
  }

}