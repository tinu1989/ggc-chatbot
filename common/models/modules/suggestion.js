const algolia = require("./algolia")();

module.exports = function (suggestion) {
	const vehicleEngine = require("./vehicleEngine")(suggestion);
	var suggestionMethod = {
		checkOptions: checkOptions,

	}
	return suggestionMethod;
	function checkOptions(apiResponse, m_callback) {

    if(!apiResponse.result.parameters.model_name){
      apiResponse.result.fulfillment.speech = "For which model do you want to know the details?";
        return m_callback('', apiResponse);
    }
		else if (!apiResponse.result.parameters.options) {
      var queryParams = [];
      for(var key in apiResponse.result.parameters){
        if(apiResponse.result.parameters[key])
        queryParams.push(key);
      }
      var searchObject = { 
      disjunctiveFacets: queryParams,
      hitsPerPage: 1
    }
      helper = algolia.initAlgoliaService(config.Algolia.VEHICLE_INVENTORY_INDEX,searchObject);
      for(var i=0;i<queryParams.length;i++){
          helper.addDisjunctiveFacetRefinement(queryParams[i], apiResponse.result.parameters[queryParams[i]]);
      }
    algolia.query(helper)
     .then(function(data){
       if(data.nbHits == 0){
         apiResponse.result.fulfillment.speech ="Sorry! We can't find any detials for that combination. Can you please ask about a valid make and model combination?";
         return m_callback('', apiResponse);
       } 
      apiResponse.result.fulfillment.speech = "What would you like to know about this car? Please select an option", 
      apiResponse.result.fulfillment.payload = {"type": "button", "results": ["Similar cars", "Price"]};
      suggestion.app.models.chatHistory.addchathistory(apiResponse,'bot','incoming');
			return m_callback('', apiResponse);
     })

		}
		else if (!apiResponse.result.parameters.make_name && !apiResponse.result.parameters.model_name && apiResponse.result.parameters.model_trim_name) {
			suggestion.app.models.chatHistory.addchathistory(apiResponse,'bot','incoming'); 
			return m_callback('', apiResponse);;
		}
		else {
			switch (apiResponse.result.parameters.options) {
				case 'similarcar':
					delete apiResponse.result.parameters.options
					vehicleEngine.findSimilarCar(apiResponse, m_callback);
					break;
				case 'price':
					delete apiResponse.result.parameters.options
					apiResponse.result.parameters.InventorySearchParameters = ['gogocar_price']
					vehicleEngine.vehicleInventoryEnquiry(apiResponse, m_callback);
					break;
			}

		}
	}

}