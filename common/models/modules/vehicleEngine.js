const algolia = require("./algolia")();
var deepcopy = require("deepcopy");
var Q = require('q');
module.exports = function (vehicleEngine) {
  var searchEngine = {
    findSimilarCar: findSimilarCar,
    vehicleInventoryEnquiry: vehicleInventoryEnquiry,
    vehicleInventoryPolarEnquiry: vehicleInventoryPolarEnquiry
  }
  return searchEngine;
  function findSimilarCar(apiResponse, callBack) {
    algolia.getSimilarCar(apiResponse.result.parameters)
      .then(function (data) {
        if (data.hits[0] && data.hits[0].similar_cars) {
          apiResponse.result.fulfillment.speech = [
            {
              type: 'arraysuggestion',
              data: data.hits[0].similar_cars
            }
          ];
          apiResponse.result.fulfillment.viewType = 'similarCar'
        }
        else
          apiResponse.result.fulfillment.speech = 'Sorry! We can\'t find any similar cars';
        vehicleEngine.app.models.chatHistory.addchathistory(apiResponse, 'bot', 'incoming');
        return callBack('', apiResponse);
      })
      .catch(function (err) {
        console.log(err);
      })
  }
  function vehicleInventoryEnquiry(apiResponse, m_callback) {
    if (apiResponse.result.action == 'auto_engine_expert_none_polar_question') {
      if (apiResponse.result.parameters) {
        // apiResponse.result.parameters = comparePreviousChatAndGetContext(apiResponse.result.parameters);
      }
      else {
        apiResponse.result.fulfillment.speech = 'Sorry!, I don’t have enough information to answer your question. You can get started with these.';
        vehicleEngine.app.models.chatHistory.addchathistory(apiResponse, 'bot', 'incoming');
        return m_callback('', apiResponse)
      }
    }
    algolia.getVehicleInventoryInfo(apiResponse.result.parameters)
      .then(function (data) {
        data.answer = data.answer.concat([{sub_type:'link',about:'vehicleLink',data:data.payload }]);
        if (data.answer.length != 0) {
          apiResponse.result.fulfillment.speech = 'autoengine';
          apiResponse.result.fulfillment.payload = { "type": 'multiPurpose','results':data.answer};
        }
        else
          apiResponse.result.fulfillment.speech = 'Sorry!, I don’t have enough information to answer your question. You can get started with these.';
       // vehicleEngine.app.models.chatHistory.addchathistory(apiResponse, 'bot', 'incoming');
        m_callback('', apiResponse)
      })
      .catch(function (err) {
        m_callback(err);
      })
  }
  function comparePreviousChatAndGetContext(conversationObject) {
    var currentContext = {};
    for (var conversationKey in conversationObject) {
      if (conversationKey.indexOf("prev-") != -1 || conversationKey.indexOf(".original") != -1)
        continue;
      if (conversationObject[conversationKey])
        currentContext[conversationKey] = conversationObject[conversationKey];
      else if (conversationObject["prev-" + conversationKey])
        currentContext[conversationKey] = conversationObject["prev-" + conversationKey];
    }
    return currentContext;
  }


  function vehicleInventoryPolarEnquiry(apiResponse, m_callback) {
    var algoliaInstance = [],
      algoliaResult,
      copyQueryParams = deepcopy(apiResponse.result.parameters),
      searchParameters,
      features = [],
      ouputText,
      checkFeature = apiResponse.result.parameters.checkFeature,
      featuresValue = {};
    delete copyQueryParams.checkFeature;
    for (var i = 0; i < checkFeature.length; i++) {
      var key = Object.keys(checkFeature[i])[0];
      featuresValue[key] = featuresValue[key] ? featuresValue[key].concat([checkFeature[i][key]]) : [checkFeature[i][key].toString()];
      if (features.indexOf(key) == -1)
        features.push(key);
    }
    searchParameters = Object.keys(copyQueryParams).concat(features);
    var searchObject = {
      disjunctiveFacets: searchParameters,
      attributesToRetrieve: searchParameters.toString()
    };
    var helper = algolia.initAlgoliaService(config.Algolia.VEHICLE_INVENTORY_INDEX, searchObject);
    for (var key in copyQueryParams) {
      if (copyQueryParams[key]) {
        ouputText = ouputText ? ouputText + ' ' + copyQueryParams[key] : copyQueryParams[key]
        helper.addDisjunctiveFacetRefinement(key.replace('prev-', ''), copyQueryParams[key]);
      }
    }
    algoliaResult = algolia.query(helper)
    algoliaResult.then(function (data) {
      if (data.hits.length == 0)
        return m_callback('', apiResponse);
      facet = algolia.formatFacets(data.disjunctiveFacets);
      var ans = [];
      for (var feature in featuresValue)
        for (var i = 0; i < featuresValue[feature].length; i++) {
          if (facet[feature] && facet[feature].data && facet[feature].data.indexOf(featuresValue[feature][i]) != -1) {
            ans.push({ 'text': featuresValue[feature][i], status: true ,sub_type:'textwithstatus' });
          }
          else if (data.hits[0][feature]) {
            ans.push({ 'text': feature, status: data.hits[0][feature] == featuresValue[feature][i] ? true : false,sub_type:'textwithstatus'});
          }
          else
            ans.push({ 'text': featuresValue[feature][i], status: false,sub_type:'textwithstatus'  });
        }
        ans.push({sub_type:'link',about:'vehicleLink',data:copyQueryParams })
      apiResponse.result.fulfillment.speech = ouputText;
      apiResponse.result.fulfillment.payload = { "type": 'multiPurpose',"results":ans }
      vehicleEngine.app.models.chatHistory.addchathistory(apiResponse, 'bot', 'incoming');
      m_callback('', apiResponse);
    })
  }
};