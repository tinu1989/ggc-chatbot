module.exports = function (socketIO,app) {
	//rtm
	var RedisGGC = require('./redis')();
	var unirest = require("unirest");
	var Slack = require('@slack/client');
	var RtmClient = Slack.RtmClient;
	var RTM_EVENTS = Slack.RTM_EVENTS;
	var rtm = new RtmClient(global.config.SLACK_TOKEN, { logLevel: 'info' });
	rtm.start();

	rtm.on(RTM_EVENTS.MESSAGE, function (message) {
		var channel = message.channel;
		var text = message.text;
		if (channel) {
			var _this = this;
			RedisGGC.Get(channel)
				.then(function (data) {
					if (data === null) {
					} else {
						var userName;
						if (message && message.type == "message" && !message.subtype && !message.bot_id) {
							var userId = message.user;
							var redisobject = JSON.parse(data);
							unirest.get("https://slack.com/api/users.info?token=" + global.config.SLACK_TOKEN + "&user=" + userId + "&pretty=1")
								.set('Content-Type', 'application/json; charset=utf-8')
								.end(function (response) {
									userName = response.body.user.name;
									if (message && message.type == "message" && !message.subtype && !message.bot_id && message.text != "#stop") {
										socketIO.emit(redisobject.sessionID, message.text, userName);	
										var sessionid = redisobject.sessionID;
										var authid = redisobject.authID;
										var slackresobj = {
											sessionId : sessionid,
											origin : "slack",
											msg : message.text,
											viewType : "none",
											UserAuthID : authid
										}
										app.models.chatHistory.addchathistory(slackresobj,'slack','incoming');
										RedisGGC.Set(sessionid, { "channel": channel, "interruptFlag": true })
									}
									if (message && message.text == "#stop") {
										//socketIO.emit(channel,message.text);
										var sessionid = redisobject.sessionID;
										var authid = redisobject.authID;
										RedisGGC.Set(sessionid, { "channel": channel, "interruptFlag": false });
										//var slackUserEndMsg = "Hey! <br/> How can I help you? <br/> Gogo’s human teammate here.";
										var slackUserEndMsg ="Thank  you for using  GoGo Human Teammate!<br/>Now we are passing control back to GoGo, our bot. You can always request for a GoGo Human Team mate by clicking the person icon."
										//socketIO.emit(channel, slackUserEndMsg, userName);
										socketIO.emit(sessionid, slackUserEndMsg, userName,1);
									}
								})
						}
					}
				})
		}

		if (message && message.type == "message" && !message.subtype && !message.bot_id) {
		}
	});

	socketIO.on('connection', function (socket) {
		socket.on('disconnect', function () {
		});
	});


};