var app = require('../../../server/server');
var mysql = require('mysql');
var async = require("async");
var myModule = require('../../../server/boot/middleware');

module.exports = function () {
	var MostSelling = {
		findMostSellingCar: findMostSellingCar
	}
	return MostSelling;
	function findMostSellingCar(apiResponse, m_callback) {
        var originValue = myModule.originValue;
		/*if(originValue == "localhost:3000"){
          originValue = 'qa2.toyota.plaza.gogocar.com';			
		}*/
		if (apiResponse.result.parameters.searchType == "most_selling") {
			//originValue = 'qa2.toyota.plaza.gogocar.com';
			//query		
			var query1 = "SELECT dealer_public_id FROM dealer_domain WHERE domain = ?";
			var query2 = "SELECT COUNT(*) as count,make, model,trim,car_vin FROM  customer_journey WHERE dealer_public_id = ? GROUP BY make, model, trim ORDER BY COUNT(*) DESC LIMIT 1";

			async.waterfall([
				function getDealerPublicId(next) {
					var dealerPublicId;
					db.query(query1, originValue, function (err, rows) {
						if (err) throw err;
						dealerPublicId = rows[0].dealer_public_id;
						return next(null, dealerPublicId)
					});
				},
				function getMostSellingCarDetails(dealerPublicId, next) {
					var carDetails;
					db.query(query2, dealerPublicId, function (err, rows) {
						if (err) throw err;
						carDetails = rows;
						apiResponse.result.fulfillment.viewType = 'most-selling-car';
						  apiResponse.result.fulfillment.speech = [
							{
							  "maintext": "This is the most selling car",
							  "type": "mostsellingcar",
							  "data": [
								{
								  "make_name": carDetails[0].make,
								  "model_name": carDetails[0].model,
								  "vin": carDetails[0].car_vin,
								  "trim_name": carDetails[0].trim			
								}
							  ]
							}
						  ];
						return m_callback(null, apiResponse)
					});
				}
			],
				function (err, res) {
					if (err)
						m_callback(1);
					else
						m_callback("", res);
				});
		} else if (apiResponse.searchType == "latest_model") {
			//query		
			var query1 = "SELECT dealer_public_id FROM dealer_domain WHERE domain = ?";
			var query2 = "SELECT COUNT(*),make, model,TRIM,YEAR,car_vin FROM  customer_journey WHERE dealer_public_id = ? GROUP BY make, model, TRIM, YEAR ORDER BY YEAR DESC, COUNT(*) DESC";

			async.waterfall([
				function getDealerPublicId(next) {
					var dealerPublicId;
					db.query(query1, originValue, function (err, rows) {
						if (err) throw err;
						dealerPublicId = rows[0].dealer_public_id;
						return next(null, dealerPublicId)
					});
				},
				function getLatestModel(dealerPublicId, next) {
					var carDetails;
					db.query(query2, dealerPublicId, function (err, rows) {
						if (err) throw err;
						carDetails = rows;
						return m_callback(null, carDetails)
					});
				}
			],
				function (err, res) {
					if (err)
						m_callback(1);
					else
						m_callback("", res);
				});
		}
	}
};