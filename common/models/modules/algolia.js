var Q = require('q');
var _ = require('lodash');
var deepcopy = require("deepcopy");
var algoliasearch = require('algoliasearch');
var algoliasearchHelper = require('algoliasearch-helper');
var answerEngine = require("./answerRuleEngine")();
var async = require("async");
var SearchParameters = ['InventorySearchParameters', 'checkFeature'];
var answerFormatingRule = {
  "gogocar_price": {
    name: "Price",
    resultFieldTobeConsider: 'stats',
    type: "N",
    elementTobeConsidered: 'min'
  }
}
module.exports = function () {
  var Algolia = {
    getSimilarCar: getSimilarCar,
    getVehicleInventoryInfo: getVehicleInventoryInfo,
    algoliaQuery: algoliaQuery,
    initAlgoliaService: initAlgoliaService,
    addSearchParameters: addSearchParameters,
    query: query,
    formatFacets: formatFacets,
    getNumericalFromAlgolia: getNumericalFromAlgolia,	
    algoliaSearch: algoliaSearch
  };
  return Algolia;
  
  function getNumericalFromAlgolia(data) {
    var resultSet = [];
    var atributeToRetrive = ['make_name', 'model_name', 'model_trim_name', 'model_year', 'gogocar_price', 'vin', 'dataone_image_path', 'city_mpg', 'highway_mpg', 'msrp', 'savings'];
    var deferred = Q.defer();

    var searchObject = {
      facets: ['model_name', 'make_name'],
      attributesToRetrieve: atributeToRetrive,
      //distinct: true
    };
    searchObject.facets.push('car_usage_type');
    var helper = initAlgoliaService(global.config.Algolia.VEHICLE_INVENTORY_INDEX, searchObject)
    helper.addFacetRefinement('car_usage_type', 'New');
    if (data && data[0].value && data[0].condition != ">= AND <=") {
      data[0].value = data[0].value.replace(/\D+/g, '');
    }

    if (data && data[0].condition && data[0].value && data[0].type) {
      if (data[0].condition == ">" || data[0].condition == "<") {
		if(data[0].type != "mpg"){  //city_mpg, highway_mpg, price
		  helper.addNumericRefinement(data[0].type, data[0].condition, data[0].value);
		}else{
          helper.setQueryParameter('filters', 'highway_mpg '+data[0].condition+''+data[0].value);		  
		}
		
      } else if (data[0].condition == ">= AND <=" && data[0].value && data[0].type) {
        for (var i = 0; i < data[0].value.length; i++) {
          data[0].value[i] = data[0].value[i].replace(/\D+/g, '');
        }
        if (data[0].value instanceof Array) {  //check array or not
          helper.setQueryParameter('filters', data[0].type+'>'+data[0].value[0] + ' AND '+ data[0].type+'<'+ data[0].value[1]);
        } else {
          helper.setQueryParameter('filters', data[0].type+'='+ data[0].value.replace(/\D+/g, ''));
        }
      }
    }
	
	//savings
	if(data && data[0].type == "savings" && !data[0].condition){
		if(data[0].value){
		  helper.setQueryParameter('filters', data[0].type+'='+ data[0].value);
	  	}
		if(data[0].make_name){
		  helper.addFacetRefinement('make_name', data[0].make_name);	
		}else if(data[0].model_name){
		  helper.addFacetRefinement('model_name', data[0].model_name);	
		}
	}
	
    helper.search();
    helper.on('result', function (response) {
      if (response.facets.length != 0) {
        //var facetsLength = Object.keys(response.facets[0].data).length;
        var algoliaReq = [], algoliaRes = [], modelName = [];
        //fetch modelname
        for (var i in response.facets[0].data) {
          modelName.push(i);
        }
        async.each(modelName, function (model, callback) {
          var helper = initAlgoliaService(global.config.Algolia.VEHICLE_INVENTORY_INDEX, searchObject)
          helper.addFacetRefinement('car_usage_type', 'New');
		  
		  //savings
		  if(data && data[0].type == "savings"){
			if(data[0].value){  
			  helper.setQueryParameter('filters', data[0].type+'='+ data[0].value);
			}
			if(data[0].make_name){
			  helper.addFacetRefinement('make_name', data[0].make_name);	
			}else if(data[0].model_name){
			  helper.addFacetRefinement('model_name', data[0].model_name);	
			}
		  }

          if (data && data[0].value && data[0].condition != ">= AND <=") {
            data[0].value = data[0].value.replace(/\D+/g, '');
          }

          if (data && data[0].condition && data[0].value && data[0].type) {
            if (data[0].condition == ">" || data[0].condition == "<") {
              //helper.addNumericRefinement(data[0].type, data[0].condition, data[0].value);
			  if(data[0].type != "mpg"){  //city_mpg, highway_mpg, price
		        helper.addNumericRefinement(data[0].type, data[0].condition, data[0].value);
		      }else{
                helper.setQueryParameter('filters', 'highway_mpg '+data[0].condition+''+data[0].value);		  
		      }
            } else if (data[0].condition == ">= AND <=") {
              for (var i = 0; i < data[0].value.length; i++) {
                data[0].value[i] = data[0].value[i].replace(/\D+/g, '');
              }

              if (data[0].value instanceof Array) {  //check array or not
                helper.setQueryParameter('filters', data[0].type+'>'+data[0].value[0] + ' AND '+ data[0].type+'<'+ data[0].value[1]);
              } else {
                helper.setQueryParameter('filters', data[0].type+'='+ data[0].value.replace(/\D+/g, ''));
              }
            }
          }

          helper.addFacetRefinement('model_name', model); //fecth data modelwise
          helper.search();
          helper.on('result', function (response) {
            if (response && response.hits.length != 0) {
              response.hits[0].subtype = "card";
              resultSet.push(response.hits[0]);
            }

            if (modelName.length == resultSet.length) {
              if (resultSet) {
                return deferred.resolve(resultSet);
              }
              return deferred.resolve(null);
            }
          });
          // Event listener invoking is error
          helper.on('error', function (data) {
            return deferred.reject(data);
          });
          return deferred.promise;
        });
      }
    });
    // Event listener invoking is error
    helper.on('error', function (data) {
      return deferred.reject(data);
    });
    return deferred.promise;
  }  
  
  function getSimilarCar(data) {
    var deferred = Q.defer();
    var details = {};
    var client = algoliasearch(global.config.Algolia.ALGOLIA_CLIENT_ID, global.config.Algolia.ALGOLIA_TOKEN);
    var acceptParams = {
      'make_name': true,
      'model_name': true,
      'model_year': true
    };
    var helper = algoliasearchHelper(client, global.config.Algolia.SIMILARCAR_INDEX, {
      disjunctiveFacets: ['make_name', 'model_name', 'model_year'],
      attributesToRetrieve: 'similar_cars',
      hitsPerPage: 1
    });
    // Event listener invoking once data received
    helper.on('result', function (data) {
      return deferred.resolve(data);
    });
    // Event listener invoking is error
    helper.on('error', function (data) {
      return deferred.reject(data);
    });
    helper.clearRefinements();
    for (var key in data) {
      if (acceptParams[key])
        helper.addDisjunctiveFacetRefinement(key, data[key]);
    }


    helper.search();
    return deferred.promise;
  }

  function getVehicleInventoryInfo(queryParams, context) {
    var deferred = Q.defer();
    var copyQueryParams = deepcopy(queryParams);
    var searchObject = {
      facets: addSearchParameters(copyQueryParams),
      attributesToRetrieve: queryParams.InventorySearchParameters.toString(),
    };
    searchObject.facets.push('car_usage_type');
    var helper = initAlgoliaService(global.config.Algolia.VEHICLE_INVENTORY_INDEX, searchObject)
    helper.addFacetRefinement('car_usage_type', 'new');
    for (var filter in copyQueryParams) {
      if (filter.indexOf(".original") === -1)
        helper.addFacetRefinement(filter, copyQueryParams[filter]);
    }
    helper.search();
    helper.on('result', function (successRes) {
      var answer = [];
      if (successRes.facets.length != 0) {
        var factes = formatFacets(successRes.facets);
        for (var questionIndex in queryParams.InventorySearchParameters) {
          question = queryParams.InventorySearchParameters[questionIndex];
          if (answerEngine.getAnswer(question, factes, copyQueryParams, successRes.hits[0]))
            answer.push(answerEngine.getAnswer(question, factes, copyQueryParams, successRes.hits[0]));
        }
        copyQueryParams 
        return deferred.resolve({ "answer": answer, "payload": copyQueryParams });
      }
      return deferred.resolve({ "answer": answer });
    });
    // Event listener invoking is error
    helper.on('error', function (data) {
      return deferred.reject(data);
    });
    return deferred.promise;
  }


  function algoliaQuery(filterParameter) {

  }

  function initAlgoliaService(algoliaIndex, searchParams) {
    var ctx = LoopBackContext.getCurrentContext();
    var algoliaToken = ctx && ctx.get('algoliaToken');
    var client = algoliasearch(global.config.Algolia.ALGOLIA_CLIENT_ID, algoliaToken || global.config.Algolia.ALGOLIA_TOKEN);
    return algoliasearchHelper(client, algoliaIndex, searchParams);
  }

  function addSearchParameters(queryParams) {
    var arrayOfqueryParams = Object.keys(queryParams);
    for (var i = 0 in SearchParameters) {
      if (arrayOfqueryParams.indexOf(SearchParameters[i]) != -1) {
        arrayOfqueryParams = arrayOfqueryParams.concat(queryParams[SearchParameters[i]]);
        delete queryParams[SearchParameters[i]];
      }
    }
    return arrayOfqueryParams;
  }

  function query(helperObj) {
    var deferred = Q.defer();
    helperObj.search();
    helperObj.on('result', function (data) {
      return deferred.resolve(data);
    });
    // Event listener invoking is error
    helperObj.on('error', function (data) {
      return deferred.reject(data);
    });
    return deferred.promise;
  }



  function formatFacets(facetObject) {
    var formatedFacet = {};
    for (var i = 0; i < facetObject.length; i++) {
      formatedFacet[facetObject[i].name] = {
        data: Object.keys(facetObject[i].data),
        stats: facetObject[i].stats
      }
    }
    return formatedFacet;
  }


  function algoliaSearch(queryParams, context) {
    var deferred = Q.defer();
    var copyQueryParams = deepcopy(queryParams);
    var searchObject = {
      facets: addSearchParameters(copyQueryParams),
      attributesToRetrieve: queryParams.InventorySearchParameters.toString(),
      hitsPerPage: 1,
      aroundRadius: 100000000000000000,
      aroundLatLngViaIP: true,
      getRankingInfo: true
    };
    searchObject.facets.push('car_usage_type');
    var helper = initAlgoliaService(global.config.Algolia.VEHICLE_INVENTORY_INDEX, searchObject)
    helper.addFacetRefinement('car_usage_type', 'new');
    for (var filter in copyQueryParams) {
      if (filter.indexOf(".original") === -1)
        helper.addFacetRefinement(filter, copyQueryParams[filter]);
    }
    helper.search();
    helper.on('result', function (successRes) {
      var answer = [];
      if (successRes.facets.length != 0) {
        var factes = formatFacets(successRes.facets);
        successRes["factes"] = factes;
        return deferred.resolve(successRes);
      }
      return deferred.resolve(null);
    });
    // Event listener invoking is error
    helper.on('error', function (data) {
      return deferred.reject(data);
    });
    return deferred.promise;
  }


  // function formatFacets(facetObject){
  //   var formatedFacet = {};
  //  for(var i=0;i<facetObject.length;i++){
  //    if(answerFormatingRule[facetObject[i].name] && answerFormatingRule[facetObject[i].name].type == 'N')
  //    {  var answer = facetObject[i][answerFormatingRule[facetObject[i].name].resultFieldTobeConsider];
  //       formatedFacet[facetObject[i].name] = ["starts from " + answer.min];
  //    }
  //    else
  //    formatedFacet[facetObject[i].name] = Object.keys(facetObject[i].data);
  //  }
  //  return formatedFacet;
  // } 
};