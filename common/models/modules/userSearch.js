const algolia = require("./algolia")();
const jsonObject = {
  "make_name": "Select Your Brand",
  "model_name": "Select Your Model",
  "model_year": "Select Your Year",
  "model_trim_name": "Select Your Trim"
}
module.exports = function () {
  var userSearch = {
    "getMycar": getMycar
  }
  return userSearch;
  function getMycar(apiResponse, m_callback) {
    var InventorySearchParameters;
    var searchObj = {}, finishFlag = 1;
    for (var key in apiResponse.result.parameters) {
      if (!apiResponse.result.parameters[key]) {
        InventorySearchParameters = key;
        finishFlag = 0;
        break;
      }
      else
        searchObj[key] = apiResponse.result.parameters[key];
    }
    if (!finishFlag)
      searchObj["InventorySearchParameters"] = [InventorySearchParameters];
    switch (finishFlag) {
      case 0:
        algolia.algoliaSearch(searchObj)
          .then(function (data) {
            if (data.nbHits > 0) {
              apiResponse.result.fulfillment.speech = jsonObject[InventorySearchParameters];
              apiResponse.result.fulfillment.payload = {'type':'button','results':data["factes"][InventorySearchParameters].data}
            }
            else {

            }
            return m_callback('', apiResponse);
          })
        break;
      case 1:
        apiResponse.result.fulfillment.speech = 'We found following result for you';
        apiResponse.result.fulfillment.payload = {'type':'multiPurpose', 'results':[{sub_type:'link' ,about:'vehicleLink',data: searchObj}]};
        m_callback('', apiResponse);
        break;
    }
  }
}