var Q = require ('q');

module.exports = function () {
  var RedisGGC = {
    Set: SetValue,
    Get: GetValue,
    Delete: DeleteValue,
    isExist: iskeyExist,
    ExecuteCommand: ExecuteCommand
  };
  return RedisGGC;
  /**
   *
   * @constructor
   */
  // , 'EX', 10
  function SetValue(key, data,exp)
  { 
    var deferred = Q.defer ();
    if(exp && exp > 0){
      Redisclient.set(key, JSON.stringify(data),'EX',exp, function(err, reply) {
        if (err) {
          return deferred.reject (err);
        }
        return deferred.resolve (reply);
      });
    }else{
      Redisclient.set(key, JSON.stringify(data), function(err, reply) {
        if (err) {
          return deferred.reject (err);
        }
        return deferred.resolve (reply);
      });
    }
    return deferred.promise;
  }

  /**
   *
   * @constructor
   */
  function GetValue(key)
  {
    var deferred = Q.defer ();
    Redisclient.get(key, function(err, reply) {
      if (err) {
        return deferred.reject (err);
      }
      return deferred.resolve (reply);
    });
    return deferred.promise;
  }

  /**
   *
   */
  function iskeyExist(key)
  {
    var RedisMulti = Redisclient.multi();
    var deferred = Q.defer ();
    RedisMulti.exists(key);
    RedisMulti.exec(function (err, replies) {
      if (err) {
        return deferred.reject(0);
      } // end if
      if (replies[0]) {
        return deferred.resolve(key);
      } else {
        return deferred.resolve(0);
      } // end if
    });
    return deferred.promise;
  }

  /**
   *
   * @constructor
   */
  function DeleteValue(key)
  {
    var RedisMulti = Redisclient.multi();
    var deferred = Q.defer ();
    RedisMulti.del(key);
    RedisMulti.exec(function (err, replies) {
      if (err) {
        return deferred.reject(0);
      } // end if
      if (replies[0]) {
        return deferred.resolve(key);
      } else {
        return deferred.resolve(0);
      } // end if
    });
    return deferred.promise;
  }

  /**
   * 
   */
  function ExecuteCommand(cmd, params)
  {
    var deferred = Q.defer ();
    Redisclient.send_command(cmd, params, function(err, reply) {
      if (err) {
        return deferred.reject (err);
      }
      return deferred.resolve (reply);
    });
    return deferred.promise;
  }
};