var sg = require('sendgrid')(global.config.SENDGRID_API_KEY);
var helper = require('sendgrid').mail;
var dynamoDBModel    = require('./dynamoDBConnection')();
var randomstring = require("randomstring");
var useragent = require('useragent');

module.exports = function () {
	var suggestionMethod = {
		createTicket: createTicket,
		sendEmail:sendEmail,
        formatApiaidata:formatApiaidata,
        getDateFormatted: getDateFormatted
	}
	return suggestionMethod;
	function formatApiaidata(response,userInfo,req,m_callback){

		var data = {
						issue_desciption:response.result.parameters.issue_desciption,
						issue_frequency:response.result.parameters.issue_frequency,
						severity_level:response.result.parameters.severity_level,
						ticketnumber:randomstring.generate(7)	
					}
		response.result.fulfillment.speech = response.result.fulfillment.speech.replace("#ticketnumber#","<b>"+data.ticketnumber+"</b>");
		m_callback("", response)	
		sendEmail(data,userInfo,req,function(err,res){
			
		});
	}
	
	function getDateFormatted(str) {
        var daysNames = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
          monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
          d = new Date(str),
          day = d.getDate(),
          month = monthNames[d.getMonth()],
          year = d.getFullYear().toString(),
          hours = d.getHours(),
          minutes = d.getMinutes(),
          dayName = daysNames[d.getDay()];
        return hours > 12 ? (dayName + ' ' + month + ' ' + day + ',' + year + ' ' + (hours - 12) + ":" + minutes + " PM"):
          (dayName + ' ' + month + ' ' + day + ' ' + hours + ":" + minutes + " AM");
    }
	
	function sendEmail(data,userInfo,req,m_callback){

		//var ip = (req.headers['x-forwarded-for'] || req.connection.remoteAddress)				 

		var agent = useragent.parse(req.headers['user-agent']);
		var os=agent.os.toString();
		var browser=agent.toAgent();

		if(!data.ticketnumber)
			var ticketnumber=randomstring.generate(7);
		else
			var ticketnumber=data.ticketnumber;

		dynamoDBModel.insert({
		  id: Date.now()+"_"+ticketnumber,
          ticketnumber:  ticketnumber,
          issue_desciption:data.issue_desciption,
          issue_frequency: data.issue_frequency,
          severity_level: data.severity_level,
          userInfo:JSON.stringify(userInfo),
          data:JSON.stringify(data),
          createdtime:Date.now(),
          browser:browser,
          Os:os,
          Referer:req.headers.referer,
        }, global.config.DYNAMODB_PREFIX + "support-ticket")
          .then(function (res) {

        });
        var formatedDate = getDateFormatted(Date.now()); 		
		
		var msg="Issue Details : "+data.issue_desciption+"<br/>Issue Frequency : "+data.issue_frequency+"<br/>Severity Level : "+data.severity_level+"<br/>Ticket Number : "+ticketnumber;
		if(userInfo && userInfo.UserEmail){
			msg+="<br/>User Email : "+userInfo.UserEmail;
			msg+="<br/>User FirstName : "+userInfo.UserFirstName;
			msg+="<br/>User LastName : "+userInfo.UserLastName;
			msg+="<br/>User Phone : "+userInfo.UserPhone;
		}
		msg+="<br/>Current Url : "+req.headers.referer;
		msg+="<br/>User Browser : "+browser;
		msg+="<br/>User Os : "+os;
		msg="<br/><b>"+msg+"</b>";
        msg+="<br/>Date : "+formatedDate;

		var mail = new helper.Mail();		
		var email = new helper.Email("support@gogocar.com", "Gogocar");
		mail.setFrom(email);
		mail.setSubject("Support Ticket");
		var personalization = new helper.Personalization();
		email = new helper.Email("support@gogocar.com", "Gogocar");
		personalization.addTo(email);
		mail.addPersonalization(personalization);
		var content = new helper.Content('text/html', '<html><body>'+msg+'</body></html>');
		//var content = new helper.Content('text/plain', msg);
		mail.addContent(content);
		var request = sg.emptyRequest({
		  method: 'POST',
		  path: '/v3/mail/send',
		  body: mail.toJSON(),
		});
		sg.API(request, function(err, response) {
		  if(err){
		  	m_callback({error:1,message:"error occured",ticketnumber:ticketnumber});
		  }else{
		  	m_callback(null,{error:0,ticketnumber:ticketnumber});
		  }
		});

               
	}
	function createTicket(data,m_callback){

		/*console.log(response);
		var data = {
				    "fields": {
				       "project":
				       { 
				          "key": "GDE"
				       },
				       "summary": "Support Ticket -"+data.issue_summarize,
				       "description": data.issue_desciption+"-------"+"issuefrequency="+data.issue_frequency+"-------"+"severity level="+data.severity_level,
				       "issuetype": {
				          "name": "Bug"
				       }
				   }
				};

		  unirest.get("https://innovationincubator.atlassian.net/rest/api/2/issue/")
	        .set('Content-Type', 'application/json; charset=utf-8')
	        .header("Authorization", "Basic bmlzc2FudGguc0BpaW5lcmRzLmNvbTpzb2Z0aW5jQDE5ODU=")
	        .send(data)
	        .end(function (response) {
	        	return m_callback("", response)
	        });
		*/
	}

};
