var unirest=require("unirest");
module.exports = function(){
    var userObFUnction = {
        generateUserContext : generateUserContext,
        setUserInfoContext : setUserInfoContext
    }
    return userObFUnction
    function generateUserContext(userObject){
        if(!userObject)
            return null;
        if(userObject.UserFirstName && userObject.UserFirstName.indexOf('.')!=-1)
            userObject.UserFirstName = userObject.UserFirstName.replace('.', ' ') 
        var userObj = [
            {
                "name": userObject.UserRoleID ==3 ? "userInfo" : "DealerInfo",
                "parameters":{"user-name":userObject.UserFirstName},
                "lifespan":  userObject.UserRoleID ==3 ? 2 : 100
            }
        ] 
        return userObj;
    }

    function setUserInfoContext(sessionID,userInfo,next){
            userInfo = userObFUnction.generateUserContext(userInfo)
        	unirest.post("https://api.api.ai/v1/contexts?sessionId="+sessionID)
                .set('Content-Type', 'application/json') 
                .header("Authorization","Bearer "+global.config.DEVELOPER_TOKEN)          
                .send(userInfo)
                .end(function (response) {
                    if(next) 
                      next();
                })
    }
}