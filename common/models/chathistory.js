'use strict';
var randomstring = require("randomstring");
var async = require("async");
var unirest = require("unirest");
var dynamoDBModel = require('./modules/dynamoDBConnection')();

module.exports = function (chatHistory) {
	var setMethodsVisibility = function (Model, methods) {
		methods = methods || [];
		Model.sharedClass.methods().forEach(function (method) {
			method.shared = methods.indexOf(method.name) > -1;
		});
	};
	setMethodsVisibility(chatHistory);

	chatHistory.addchathistory = function (apiResponse, fromtype, reqtype, m_callback) {
		if (apiResponse) {

			var now = new Date();
			var time = now.getTime();
			var offset = now.getTimezoneOffset();
			offset = offset * 60000;
			var utctime = time - offset;

			var ctx = LoopBackContext.getCurrentContext();
			var origin = ctx && ctx.get('origin');
			var authid = ctx && ctx.get('authid');

			if (fromtype == 'slack') {
				var viewtype = apiResponse.viewType;
				var content = apiResponse.msg;
				var sessionid = apiResponse.sessionId;
				var env = apiResponse.origin;
			}
			else if (apiResponse.result && apiResponse.result.fulfillment.speech) {
				var viewtype = apiResponse.result.fulfillment.viewType;
				var content = apiResponse.result.fulfillment.speech;
				var sessionid = apiResponse.sessionId;
				var env = origin;
			}
			else {
				var viewtype = null;
				var content = apiResponse.body.msg;
				var sessionid = apiResponse.body.sessionID;
				var env = apiResponse.headers.origin;
			}

			var messageobj = {
				fromType: fromtype,
				content: content,
				viewType: viewtype
			};

			var historyparams = {
				//id:Date.now()+randomstring.generate(7),
				id: sessionid,
				sessionid: sessionid,
				message: messageobj,
				type: reqtype,
				time: utctime,
				env: env
			};

			if (apiResponse.result && apiResponse.result.fulfillment.speech) {
				historyparams.authid = authid;
				if (authid) {
					historyparams.id = authid;
				} else {
					historyparams.id = sessionid;
				}

			} else if (apiResponse.UserAuthID) {
				historyparams.authid = apiResponse.UserAuthID;
				historyparams.id = apiResponse.UserAuthID;
			}
			else if (apiResponse.body) {
				if (apiResponse.body.userInfo) {
					if (apiResponse.body.userInfo.UserAuthID) {
						historyparams.authid = apiResponse.body.userInfo.UserAuthID;
						historyparams.id = apiResponse.body.userInfo.UserAuthID;
					}
				}

			}
			dynamoDBModel.insert(historyparams, global.config.DYNAMODB_PREFIX + "chat-history")
				.then(function (data) {
					m_callback("", { error: 0, message: "successfully updated chat history", data: data });

				});
		}
	};

	// chatHistory.readchathistory=function(readparam, m_callback) {
	// 	if(readparam){
	// 	  var getparam = {
	// 		TableName: global.config.DYNAMODB_PREFIX+"chat-history",
	// 		};
	// 		if(readparam.authid){
	// 			getparam.FilterExpression= 'authid = :authid';
	// 			getparam.ExpressionAttributeValues={':authid' : readparam.authid};
	// 		}else{
	// 			getparam.FilterExpression= 'sessionid = :sessionid';
	// 			getparam.ExpressionAttributeValues={':sessionid' : readparam.sessionID};
	// 		}
	// 	  	dynamoDBModel.scanItem(getparam).then(function (response) {
	// 		console.log('response:'+response.length);
	//        return m_callback("",response);
	//       })
	//       .catch(function (err) {
	//        return m_callback(err);
	//       }); 
	// 	}
	// };

	chatHistory.readchathistory = function (readparam, m_callback) {
		var now = new Date();
		var time = now.getTime();
		var offset = now.getTimezoneOffset();
		offset = offset * 60000;
		var utctime = time - offset;

		if (readparam) {
			var getparam = {
				TableName: global.config.DYNAMODB_PREFIX + "chat-history",
				FilterExpression: "id = :idvalue",
			};
		};

		if (readparam.UserAuthID) {
			getparam.ExpressionAttributeValues = {
				":idvalue": readparam.UserAuthID
			}
		} else {
			getparam.ExpressionAttributeValues = {
				":idvalue": readparam.sessionID
			}
		}

		getparam.ScanIndexForward = false;
		//getparam.PageSize = 1;

		dynamoDBModel.scanItem(getparam).then(function (response) {
			if (response.Items.length > 0) {
				response.Items.push({ message: { fromType: 'bot', content: "Hi Welcome back" }, time: utctime });
			}
			else {
				var response = {
					Items: [{
						message: { fromType: 'bot', content: "Hi there, I'm GoGo! An assistant for your automotive needs. How can I help you ?" },
						time: utctime
					}]
				}
			}
			return m_callback("", response);
		})
			.catch(function (err) {
				return m_callback(err);
			});

	};

	chatHistory.clearchathistory = function (readid, m_callback) {

		var getparams = {
			"TableName": global.config.DYNAMODB_PREFIX + "chat-history", 
			KeyConditionExpression: "id = :id",	 
			ExpressionAttributeValues: {        
				":id":{"S":readid}  
			},
			ProjectionExpression:'id , #time',
			ExpressionAttributeNames :{
				"#time" : "time"
			},
			 ScanIndexForward : false,
			 Limit:2
		}

		dynamoDBModel.queryGetDelItem(getparams).then(function (response) {
			console.log('query get request Success ...');
				BatchDeletes(response);	
			}, function (err) {
				console.log(err);
		});
		function BatchDeletes(deleteobj){
			var tablename = global.config.DYNAMODB_PREFIX+"chat-history";
			var batchRequest = {
				RequestItems: { }	
			};
			batchRequest.RequestItems[tablename]=deleteobj;
			
			dynamoDBModel.batchWriteItem(batchRequest).then(function (response) {
				console.log('Batch delete successful ...');	
			});
		}
		
	}

	chatHistory.remoteMethod('readchathistory', {
		accepts: [{ arg: 'historyobject', type: 'object', required: true, http: { source: 'body' } }],
		http: { path: '/readchathistory', verb: 'POST' },
		returns: { type: 'JSON', root: true }
	});
	chatHistory.remoteMethod('clearchathistory', {
		accepts: [{ arg: 'clearbject', type: 'object', required: true, http: { source: 'body' } }],
		http: { path: '/clearchathistory', verb: 'POST' },
		returns: { type: 'JSON', root: true }
	});

}