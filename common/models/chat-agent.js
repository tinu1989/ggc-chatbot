'use strict';
var apiai = require('apiai');
var randomstring = require("randomstring");
var async = require("async");
var unirest = require("unirest");
module.exports = function (Chatagent) {
  var dynamoDBModel = require('./modules/dynamoDBConnection')();
  var vehicleEngine = require('./modules/vehicleEngine')(Chatagent);
  var numericalEngine= require('./modules/numericalEngine')(Chatagent);  
  var suggestion = require('./modules/suggestion')(Chatagent);
  var slack = require('./modules/slackIntegration')();
  var userContext = require('./modules/setUserContext')();
  //var mostSellingCars = require('./modules/mostSellingCar')();
  var recommendation = require('./modules/recommendation/recommendation')(Chatagent);
  var RedisGGC = require('./modules/redis')();
  var userSearch = require('./modules/userSearch')();
  var supportTicket = require('./modules/supportTicket')();
  var setMethodsVisibility = function (Model, methods) {
    methods = methods || [];
    Model.sharedClass.methods().forEach(function (method) {
      method.shared = methods.indexOf(method.name) > -1;
    });

  };
  setMethodsVisibility(Chatagent);
  // remote method after hook
  Chatagent.afterRemote('chatBottext', function (context, remoteMethodOutput, next) {
    console.log(remoteMethodOutput);
    next();
  })

  Chatagent.updateAlreadyExistingIntent = function (questondata, m_callback) {
    if (questondata && questondata.intentId && questondata.question && questondata.question != "") {
      var intentId = questondata.intentId;
      unirest.get(global.config.API_AI_URL + "intents/" + intentId)
        .set('Content-Type', 'application/json; charset=utf-8')
        .header("Authorization", "Bearer " + global.config.DEVELOPER_TOKEN)
        .end(function (response) {
          if (response && response.body && response.body.userSays) {

            var currentmessage = {
              "data": [
                {
                  "text": questondata.question
                }
              ],
              "isTemplate": false,
              "count": 1
            };
            response.body.userSays.push(currentmessage);
            console.log(JSON.stringify(response.body));
            unirest.put(global.config.API_AI_URL + "intents/" + intentId)
              .set('Content-Type', 'application/json; charset=utf-8')
              .header("Authorization", "Bearer " + global.config.DEVELOPER_TOKEN)
              .send(response.body)
              .end(function (response) {
                if (response && response.body) {
                  m_callback(null, { success: 1, message: "intent updated successfully" });
                } else {
                  m_callback({ error: 1, err: "error occured" });
                }
              });
          } else {
            m_callback({ error: 1, err: "Intent not exist" });
          }

        });
    } else {
      m_callback({ error: 1, err: "question is missing" });
    }
  };
  Chatagent.createQuestion = function (questondata, m_callback) {
    if (questondata && questondata.question && questondata.question != "" && questondata.answer && questondata.answer != "") {
      var questionformatted = questondata.question.replace(/ /g, '_');
      var intentjson = {
        "name": questionformatted,
        "auto": true,
        "contexts": [],
        "templates": [],
        "userSays": [
          {
            "data": [
              {
                "text": questondata.question
              }
            ],
            "isTemplate": false,
            "count": 0

          },
        ],
        "responses": [
          {
            "resetContexts": false,
            "affectedContexts": [],
            "parameters": [],
            "speech": questondata.answer
          }
        ],
        "priority": 500000
      };

      unirest.post(global.config.API_AI_URL + "intents")
        .set('Content-Type', 'application/json; charset=utf-8')
        .header("Authorization", "Bearer " + global.config.DEVELOPER_TOKEN)
        .send(intentjson)
        .end(function (response) {
          if (response.body && response.body.id) {
            dynamoDBModel.insert({
              question_key: Date.now() + randomstring.generate(7),
              question: questondata.question,
              answer: questondata.answer,
              intentId: response.body.id
            }, 'faq_question')
              .then(function (data) {
                m_callback("", { error: 0, message: "successfully updated", data: data });

              })
          } else {
            m_callback({ error: 1, err: response.error });
          }
        });
    } else {
      m_callback({ error: 1, err: "question/answer is missing" });
    }
  };
  Chatagent.checkChatStatus = function (data, m_callback) {
    var userSession = data.sessionID;
    RedisGGC.Get(userSession)
      .then(function (data) {
        if (data && data != null) {
          data = JSON.parse(data);
          if (data.interruptFlag == true) {
            return m_callback(null, { "botenabled": false });
          }
        }
        return m_callback(null, { "botenabled": true });
      });
  };
  Chatagent.startBotchat = function (data, m_callback) {

    var userSession = data.sessionID;
    var userInfo = data.userInfo;
    var ctx = LoopBackContext.getCurrentContext();
    var channel = ctx && ctx.get('Channel');

    RedisGGC.Get(userSession)
      .then(function (data) {
        if (data && data != null) {
          var data = JSON.parse(data);
          channel = data.channel;
          userInfo = data.userInfo;
        }
        m_callback(null, { "channel": channel, message: "Thank  you for using  GoGo Human Teammate!<br/>Now we are passing control back to GoGo, our bot. You can always request for a GoGo Human Team mate by clicking the person icon." });
        if (channel && channel != null) {
          ctx.set('Channel', channel);
          if (userInfo && userInfo.UserFirstName)
            var msg = userInfo.UserFirstName + " " + userInfo.UserLastName + " selected to chat with Gogobot";
          else
            var msg = "user selected to chat with Gogobot";
          slack.interruptsend(msg, channel);
          RedisGGC.Set(userSession, { "channel": data.channel, "interruptFlag": false });
        }
      });
  };
  Chatagent.startHumanchat = function (data, m_callback) {

    var userSession = data.sessionID;
    var userInfo = data.userInfo;
    var ctx = LoopBackContext.getCurrentContext();
    var channel = ctx && ctx.get('Channel');


    RedisGGC.Get(userSession)
      .then(function (data) {
        if (data && data != null) {
          var data = JSON.parse(data);
          channel = data.channel;
          userInfo = data.userInfo;
        }
        m_callback(null, { "channel": channel, message: "Thank you for your request,<br/>Our representative will get in touch with you shortly !" });
        if (channel && channel != null) {
          ctx.set('Channel', channel);
          if (userInfo && userInfo.UserFirstName)
            var msg = userInfo.UserFirstName + " " + userInfo.UserLastName + " want to chat with support member";
          else
            var msg = "user want to chat with support member";
          slack.interruptsend(msg, channel);
          RedisGGC.Set(userSession, { "channel": data.channel, "interruptFlag": true });
        }
      });
  };
  Chatagent.chatBottext = function (chatdata,req, m_callback) {
    var ctx = LoopBackContext.getCurrentContext();
    var userSession = ctx && ctx.get('Session');
    var channel = ctx && ctx.get('Channel');
    var userInfo = ctx && ctx.get('userInfo');
    var app = apiai(global.config.AGENT_ID);
    var request = app.textRequest(chatdata.msg, {
      sessionId: userSession
    });
    request.on('response', function (response) {
      try {
        response.channel = channel;
        console.log(response.result);
        slack.send({ "Q": chatdata.msg, "A": response.result.fulfillment.speech }, channel);
        if (response.result.metadata.intentName == "Default Fallback Intent" || response.result.metadata.intentName == "usercontextfallback") {
          var fallbackData = {
            "question_key": Date.now() + randomstring.generate(7),
            "message": response.result.resolvedQuery,
            "currentcontexts": response.result.contexts,
            "messagetime": response.timestamp,
            "fallbackstatus": 1,
            "contextname": "context_" + randomstring.generate(7),
          };
          Chatagent.fallbackApiai(fallbackData);
          RedisGGC.Get("default_intent_status_" + userSession)
            .then(function (def_count) {
              if (!def_count || def_count == null) {
                def_count = 0;
              }
              if (userInfo) {
                userContext.setUserInfoContext(userSession, userInfo)
              }
              Chatagent.app.models.chatHistory.addchathistory(response, 'bot', 'incoming');
              def_count++;
              RedisGGC.Set("default_intent_status_" + userSession, def_count, 120);
              if (def_count >= 3) {
                response.result.fulfillment.viewType = 'default-intent-human-chat';
                response.result.fulfillment.speech = '<p>would you like to chat with our support member?</p>';
                 response.result.fulfillment.payload = {"type": "button", "results": ["Start Chat"] };
                response.result.fulfillment.show_human_btn = 1;
              }
              return m_callback("", response)
            });

        }
        else if (response.result.action == 'similarcar_search')
          return vehicleEngine.findSimilarCar(response, m_callback);
        else if (response.result.action == 'auto_engine_expert_enquiry' || response.result.action == 'auto_engine_expert_none_polar_question') {
          return vehicleEngine.vehicleInventoryEnquiry(response, m_callback);
        }
        else if (response.result.action == 'similarcar-price-suggestion')
          return suggestion.checkOptions(response, m_callback);
        else if (response.result.action == 'auto_engine_expert_polar_questions')
          return vehicleEngine.vehicleInventoryPolarEnquiry(response, m_callback);
        else if (response.result.action == 'usersearch')
          return userSearch.getMycar(response, m_callback);
        else if (response.result.action == 'get-recommendation')
          return recommendation.getRecommendation(response, m_callback);
        else if (response.result.action == 'get-most-selling-car')
          return mostSellingCars.findMostSellingCar(response, m_callback);
        else if (response.result.action == 'create_support_ticket'){
          var ctx = LoopBackContext.getCurrentContext();
          var userInfo = ctx && ctx.get('userInfo');
          return supportTicket.formatApiaidata(response,userInfo,req, m_callback);
        }
        else if(response.result.action == 'numerical_questions'){
          return numericalEngine.findCar(response, m_callback);
        }
        else {
          Chatagent.app.models.chatHistory.addchathistory(response, 'bot', 'incoming');
          return m_callback("", response)
        }
      }
      catch (e) {
        console.log(e);
        return m_callback("", response)
      }
    });

    request.on('error', function (error) {
      m_callback(error);
    });

    request.end();
  };
  Chatagent.fallbackApiai = function (fallbackData) {
    dynamoDBModel.insert(fallbackData, global.config.DYNAMODB_NAME);
  };
  Chatagent.loadEntitiesdatabyId = function (entityid, m_callback) {
    unirest.get(global.config.API_AI_URL + "entities/" + entityid)
      .set('Content-Type', 'application/json; charset=utf-8')
      .header("Authorization", "Bearer " + global.config.DEVELOPER_TOKEN)
      .end(function (response) {
        if (response && response.body) {
          m_callback(null, response.body);
        } else {
          m_callback({ error: 1, err: "intent loading failed" });
        }
      });
  };
  Chatagent.loadAllEntities = function (m_callback) {
    unirest.get(global.config.API_AI_URL + "entities")
      .set('Content-Type', 'application/json; charset=utf-8')
      .header("Authorization", "Bearer " + global.config.DEVELOPER_TOKEN)
      .end(function (response) {
        if (response && response.body) {
          m_callback(null, response.body);
        } else {
          m_callback({ error: 1, err: "intent loading failed" });
        }
      });
  };
  Chatagent.loadAllintents = function (m_callback) {
    unirest.get(global.config.API_AI_URL + "intents")
      .set('Content-Type', 'application/json; charset=utf-8')
      .header("Authorization", "Bearer " + global.config.DEVELOPER_TOKEN)
      .end(function (response) {
        if (response && response.body) {
          m_callback(null, response.body);
        } else {
          m_callback({ error: 1, err: "intent loading failed" });
        }
      });
  };
  Chatagent.getFallbackQuestions = function (Lastkey, Limit, m_callback) {
    if (!Limit)
      Limit = 15;

    var params = {
      TableName: global.config.DYNAMODB_NAME,
      FilterExpression: 'fallbackstatus = :fallbackstatus',
      ExpressionAttributeValues: { ':fallbackstatus': 1 },
      Limit: Limit
    };
    if (Lastkey && Lastkey != "")
      params.ExclusiveStartKey = { "question_key": Lastkey };

    dynamoDBModel.scanItem(params).then(function (response) {
      return m_callback("", response);
    })
      .catch(function (err) {
        return m_callback(err);
      });
  };
  Chatagent.getFailedQuestions = function (Lastkey, Limit, m_callback) {

    if (!Limit)
      Limit = 15;

    var params = {
      TableName: global.config.DYNAMODB_NAME,
      FilterExpression: 'fallbackstatus = :fallbackstatus',
      ExpressionAttributeValues: { ':fallbackstatus': 2 },
      Limit: Limit,
    };
    if (Lastkey && Lastkey != "")
      params.ExclusiveStartKey = { "question_key": Lastkey };

    dynamoDBModel.scanItem(params).then(function (response) {
      return m_callback("", response);
    })
      .catch(function (err) {
        return m_callback(err);
      });
  };
  Chatagent.getAllQuestions = function (Lastkey, Limit, m_callback) {

    if (!Limit)
      Limit = 15;

    var params = {
      TableName: global.config.DYNAMODB_NAME,
      Limit: Limit,
    };
    if (Lastkey && Lastkey != "")
      params.ExclusiveStartKey = { "question_key": Lastkey };

    dynamoDBModel.scanItem(params).then(function (response) {
      return m_callback("", response);
    })
      .catch(function (err) {
        return m_callback(err);
      });
  };
  Chatagent.messagesimilaritycronejob = function (m_callback) {
    Chatagent.getFallbackQuestions("", 15, function (err, data) {

      if (!err && data.Items && data.Items.length > 0) {

        async.each(data.Items,
          function (item, callback) {

            var messagestring = item.message;
            unirest.post(global.config.SIMILARITY_ENGINE_URL + "/Similarity/Response")
              .set('Content-Type', 'application/json')
              .send({ "user_input": messagestring })
              .end(function (response) {

                if (response && response.body && response.body.intentId && (response.body.sum == "0.0" || response.body.sum == "0")) {

                  var intentId = response.body.intentId;
                  unirest.get(global.config.API_AI_URL + "intents/" + intentId)
                    .set('Content-Type', 'application/json; charset=utf-8')
                    .header("Authorization", "Bearer " + global.config.DEVELOPER_TOKEN)
                    .end(function (response) {
                      if (response && response.body) {

                        var currentmessage = {
                          "data": [
                            {
                              "text": messagestring
                            }
                          ],
                          "isTemplate": false,
                          "count": 1
                        };
                        response.body.userSays.push(currentmessage);
                        console.log(JSON.stringify(response.body));
                        unirest.put(global.config.API_AI_URL + "intents/" + intentId)
                          .set('Content-Type', 'application/json; charset=utf-8')
                          .header("Authorization", "Bearer " + global.config.DEVELOPER_TOKEN)
                          .send(response.body)
                          .end(function (response) {
                            if (response && response.body) {
                              console.log("going to dekltre");
                              var params = {
                                question_key: item.question_key,
                              };
                              console.log(params);
                              dynamoDBModel.removeItem(params, global.config.DYNAMODB_NAME).then(function (response) {
                                callback();
                              })
                                .catch(function (err) {
                                  callback();
                                });
                            } else {
                              callback();
                            }
                          });
                      }

                    });
                } else {
                  var params = {
                    "question_key": item.question_key
                  };
                  var params = "set fallbackstatus = :val";
                  var Key = {
                    "question_key": item.question_key,
                  };
                  var exp_attributes = {
                    ":val": 2
                  };
                  dynamoDBModel.updateItem(params, Key, exp_attributes, global.config.DYNAMODB_NAME).then(function (response) {
                    callback();
                  })
                    .catch(function (err) {
                      callback();
                    });
                }
              });
          }, function (err) {
            if (err)
              m_callback(err);
            else
              m_callback(null, { error: 0 });
          });
      } else if (err) {
        m_callback(err);
      } else {
        m_callback(null, { error: 0 });
      }
    });
  };
  Chatagent.createChatBotQuestion = function (question, answer, m_callback) {
    return Chatagent.createQuestion({ question: question, answer: answer }, m_callback);
  }

  Chatagent.mostSellingCar = function (data, m_callback) {
    if (data.searchType == "most_selling" || data.searchType == "latest_model") {
      return mostSellingCars.findMostSellingCar(data, m_callback);
    }
  }

  Chatagent.slackComments = function (data, m_callback) {
    var attachments = [
        {
            "text": "Choose a game to play",
            "fallback": "You are unable to choose a game",
            "callback_id": "wopr_game",
            "color": "#3AA3E3",
            "attachment_type": "default",
            "actions": [
                {
                    "name": "game",
                    "text": "Chess",
                    "type": "button",
                    "value": "chess"
                },
                {
                    "name": "game",
                    "text": "Falken's Maze",
                    "type": "button",
                    "value": "maze"
                },
                {
                    "name": "game",
                    "text": "Thermonuclear War",
                    "style": "danger",
                    "type": "button",
                    "value": "war",
                    "confirm": {
                        "title": "Are you sure?",
                        "text": "Wouldn't you prefer a good game of chess?",
                        "ok_text": "Yes",
                        "dismiss_text": "No"
                    }
                }
            ]
        }
    ];
    if (data.command) {
      var unirest = require("unirest"); unirest.post('https://slack.com/api/chat.postMessage').headers(
        { 'Content-Type': 'application/x-www-form-urlencoded' }).send(
        {
          "token": 'xoxp-220412514785-227937361076-230092312306-43a1a9a7f822a9e1e9a1a637ea2f1315',
          "channel": data.channel_id,
          'text': 'hi',
          'attachments':attachments
        }).end(function (response) { console.log(response.body); })
    }
    m_callback('', data);
  }

  Chatagent.setUserContext = function (userUnfo, m_callback) {
    var app = apiai(global.config.AGENT_ID);
    var ctx = LoopBackContext.getCurrentContext();
    var userSession = ctx && ctx.get('Session');
    var options = {
      sessionId: userSession,
      contexts: [
        {
          name: 'usercontext',
          parameters: {
            'user-name': userUnfo["user-name"]
          }
        }
      ]
    };
    var request = app.textRequest('Hello', options);
    request.on('response', function (response) {
      return m_callback("", response);
    });
    request.on('error', function (error) {
      return m_callback(error);
    });
    request.end();
  }
  Chatagent.sendEmail = function (data,req,m_callback) {
    supportTicket.sendEmail(data,data.userInfo,req,m_callback);
  };

  Chatagent.remoteMethod('checkChatStatus', {
    accepts: [{ arg: 'data', type: 'object', required: true, http: { source: 'body' } }],
    http: { path: '/checkChatStatus', verb: 'POST' },
    returns: { type: 'JSON', root: true }
  });
  Chatagent.remoteMethod('startBotchat', {
    accepts: [{ arg: 'data', type: 'object', required: true, http: { source: 'body' } }],
    http: { path: '/startBotchat', verb: 'POST' },
    returns: { type: 'JSON', root: true }
  });
  Chatagent.remoteMethod('startHumanchat', {
    accepts: [{ arg: 'data', type: 'object', required: true, http: { source: 'body' } }],
    http: { path: '/startHumanchat', verb: 'POST' },
    returns: { type: 'JSON', root: true }
  });
  Chatagent.remoteMethod('getFallbackQuestions', {
    accepts: [
      { arg: 'Lastkey', type: 'string' },
      { arg: 'limit', type: 'number' },
    ],
    http: { path: '/getFallbackQuestions', verb: 'GET' },
    returns: { type: 'JSON', root: true }
  });

  Chatagent.remoteMethod('loadEntitiesdatabyId', {
    accepts: [{ arg: 'entityid', type: 'string', require: true }],
    http: { path: '/loadEntitiesdatabyid', verb: 'GET' },
    returns: { type: 'JSON', root: true }
  });
  Chatagent.remoteMethod('loadAllEntities', {
    accepts: [],
    http: { path: '/loadAllEntities', verb: 'GET' },
    returns: { type: 'JSON', root: true }
  });
  Chatagent.remoteMethod('loadAllintents', {
    accepts: [],
    http: { path: '/loadAllintents', verb: 'GET' },
    returns: { type: 'JSON', root: true }
  });
  Chatagent.remoteMethod('getAllQuestions', {
    accepts: [
      { arg: 'Lastkey', type: 'string' },
      { arg: 'limit', type: 'number' },
    ],
    http: { path: '/getAllQuestions', verb: 'GET' },
    returns: { type: 'JSON', root: true }
  });
  Chatagent.remoteMethod('getFailedQuestions', {
    accepts: [
      { arg: 'Lastkey', type: 'string' },
      { arg: 'limit', type: 'number' },
    ],
    http: { path: '/getFailedQuestions', verb: 'GET' },
    returns: { type: 'JSON', root: true }
  });

  Chatagent.remoteMethod('updateAlreadyExistingIntent', {
    accepts: [{ arg: 'questondata', type: 'object', required: true, http: { source: 'body' } }],
    http: { path: '/updateAlreadyexistingintent', verb: 'POST' },
    returns: { type: 'JSON', root: true }
  });
  Chatagent.remoteMethod('createQuestion', {
    accepts: [{ arg: 'questondata', type: 'object', required: true, http: { source: 'body' } }],
    http: { path: '/createQuestion', verb: 'POST' },
    returns: { type: 'JSON', root: true }
  });

  Chatagent.remoteMethod('chatBottext', {
    accepts: [
      { arg: 'chatdata', type: 'object', required: true, http: { source: 'body' } },
       { arg: 'req', type: 'object', 'http': { source: 'req' } },
      ],
    http: { path: '/text', verb: 'POST' },
    returns: { type: 'JSON', root: true }
  });

  Chatagent.remoteMethod('messagesimilaritycronejob', {
    accepts: [],
    http: { path: '/messagesimilaritycronejob', verb: 'GET' },
    returns: { type: 'JSON', root: true }
  });

  Chatagent.remoteMethod('createChatBotQuestion', {
    accepts: [
      { arg: 'question', type: 'string', require: true },
      { arg: 'answer', type: 'string', require: true }
    ],
    http: { path: '/createChatBotQuestion', verb: 'POST' },
    returns: { type: 'JSON', root: true }
  });
  Chatagent.remoteMethod('mostSellingCar', {
    accepts: [
      { arg: 'domain', type: 'object', required: true, http: { source: 'body' } }],
    http: { path: '/mostSellingCar', verb: 'POST' },
    returns: { type: 'JSON', root: true }
  });
  Chatagent.remoteMethod('setUserContext', {
    accepts: [
      { arg: 'userUnfo', type: 'object', required: true, http: { source: 'body' } }],
    http: { path: '/setUserContext', verb: 'POST' },
    returns: { type: 'JSON', root: true }
  });
  Chatagent.remoteMethod('sendEmail', {
    accepts: [
      { arg: 'data', type: 'object', required: true, http: { source: 'body' } },
      { arg: 'req', type: 'object', 'http': { source: 'req' } }
      ],
    http: { path: '/sendEmail', verb: 'POST' },
    returns: { type: 'JSON', root: true }
  });

  Chatagent.remoteMethod('slackComments', {
    accepts: [
      { arg: 'data', type: 'object', required: true, http: { source: 'body' } }],
    http: { path: '/slackComments', verb: 'POST' },
    returns: { type: 'JSON', root: true }
  });

}; 
