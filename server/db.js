var mysql = require('mysql');
var connection = mysql.createConnection({
    host     : global.config.DB.host,
    user     : global.config.DB.user,
    password : global.config.DB.password,
    database : global.config.DB.database
});

connection.connect(function(err) {
    if (err){ console.log(err);	 throw err;}
    console.log('Database now connected...');
});

module.exports = connection;