var unirest = require("unirest");
var async = require("async");
//var RedisGGC = require("./redis")();
var RedisGGC = require('../../common/models/modules/redis')();
var slack = require('../../common/models/modules/slackIntegration')();
var userContext = require('../../common/models/modules/setUserContext')();
var tempstore = {};
module.exports = function (app) {
	app.use('/api/chatAgent/text', function (req, res, next) {
		try {
			if (req.body) {
				app.models.chatHistory.addchathistory(req, 'user', 'outgoing');
			}
			var originValue = req.headers.host;
			module.exports.originValue = originValue;
			loopbackContext = LoopBackContext.getCurrentContext();
			loopbackContext.set('algoliaToken', req.body.algoliaToken);
			loopbackContext.set('origin', req.headers.origin);
			if (req.body.userInfo && req.body.userInfo.UserAuthID) {
				loopbackContext.set('authid', req.body.userInfo.UserAuthID);
				loopbackContext.set('userInfo', req.body.userInfo);
				var userInfo = req.body.userInfo;
			}
			req.body.sessionID = req.body.sessionID || "test",
				loopbackContext.set('Session', req.body.sessionID);
			if (req.headers.authorization) {
				loopbackContext.set('Auth_Token', req.headers.authorization);
			} else if (req.query && req.query.access_token) {
				loopbackContext.set('Auth_Token', req.query.access_token);
			}
			if (req.body && req.body.sessionID) {
				RedisGGC.Get(req.body.sessionID)
					.then(function (data) {
						if (data === null) {
							loopbackContext.set('Session', req.body.sessionID);
							if (req.headers.authorization)
								loopbackContext.set('Auth_Token', req.headers.authorization);
							else if (req.query.access_token)
								loopbackContext.set('Auth_Token', req.query.access_token);
							var data = {};
							data.name = req.body.sessionID;   //channel name
							data.token = global.config.SLACK_TOKEN;  //token initialize
							unirest.post("https://slack.com/api/channels.create")
								.set('Content-Type', 'application/x-www-form-urlencoded')
								.send(data)
								.end(function (response) {
									if (response && response.body.ok == true && response.body && response.body.channel) {
										var channelId = response.body.channel.id;

										//fetch user list
										var data = {};
										data.token = global.config.SLACK_TOKEN;  //token initialize

										unirest.post("https://slack.com/api/users.list")
											.set('Content-Type', 'application/x-www-form-urlencoded')
											.send(data)
											.end(function (response) {
												var useridArray = [];
												var userMailId = ["ganeshkumar.s@iinerds.com", "aruna.vr@iinerds.com", "manoj.p@iinerds.com", "nissanth.s@iinerds.com", "sreekar.v@iinerds.com", "anshad@innovationincubator.com"];

												if (response && response.body.ok == true && response.body && response.body.members) {
													for (var i = 0; i < response.body.members.length - 1; i++) {
														if (userMailId.indexOf(response.body.members[i].profile.email) > -1) {
															console.log(response.body.members[i].id);
															console.log(response.body.members[i].profile.email);
															useridArray.push(response.body.members[i].id);
														}
													}
													//duplicates remove
													useridArray = useridArray.filter(function (item, index, inputArray) {
														return inputArray.indexOf(item) == index;
													});
													async.each(useridArray,
														function (userID, callback) {
															//invite members
															var data = {};
															data.channel = channelId;   //channel id
															data.user = userID;   //user id
															data.token = global.config.SLACK_TOKEN;  //token initialize

															unirest.post("https://slack.com/api/channels.invite")
																.set('Content-Type', 'application/x-www-form-urlencoded')
																.send(data)
																.end(function (response) {
																	var useridArray = [];
																	if (response && response.body.ok == true && response.body && response.body.members) {
																		console.log("members invited");
																	}
																});
														});

												}
											});

										loopbackContext.set('Channel', response.body.channel.id);
										tempstore[req.body.sessionID] = response.body.channel.id;
										RedisGGC.Set(req.body.sessionID, { "channel": response.body.channel.id, "userInfo": userInfo, "interruptFlag": false });
										var redissetobj = {
											sessionID: req.body.sessionID
										}
										if (req.body) {
											if (req.body.userInfo) {
												if (req.body.userInfo.UserAuthID) {
													redissetobj.UserAuthID = req.body.userInfo.UserAuthID;
												}
											}

										}
										RedisGGC.Set(response.body.channel.id, redissetobj);
										if (userInfo)
											return userContext.setUserInfoContext(req.body.sessionID, userInfo, next);
										return next();
									} else {
										return next();
									}
								})
						} else {
							var data = JSON.parse(data);
							loopbackContext.set('Channel', data.channel);
							if (data.interruptFlag) {
								res.send({ "channel": data.channel });
								return slack.interruptsend(req.body.msg, data.channel);
							}
							if (!data.userInfo && userInfo)
								return userContext.setUserInfoContext(req.body.sessionID, userInfo, next);
							return next();
						}
					})
					.catch(function (err) {
						return next();
					})
			}
			else {
				return next();
			}

		}
		catch (e) {
			console.log(e);
			next();
		}

	});
}