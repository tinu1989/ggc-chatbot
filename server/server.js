
var session     = require('express-session');
var loopback    = require('loopback');
LoopBackContext = require('loopback-context');
var boot        = require('loopback-boot');
var redis       = require("redis");
var app         = module.exports = loopback();
config          = require("./constant.json")[process.env.NODE_ENV || 'dev'];
/*db              = require('./db');*/

Redisclient = redis.createClient(config.REDIS_CREDENTIALS.PORT, config.REDIS_CREDENTIALS.IP);
Redisclient.on("connect", function () {
  console.log("Redis Status: OK, Select DB0: OK");
});
Redisclient.on("error", function (err) {
  console.log("Redis Status: NOT OK: "+err);
});

app.use(session({
    secret: '0`3VTw;hQ|3/`:95ZYu{0J82O>{}7JC/',
    // add redis session here if u want seperate redis store for each users
    saveUninitialized: true,
    resave: false
}));
app.use(loopback.context());
app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module){
   // app.start();
	app.io = require('socket.io')(app.start());
	var socketconnections = require('../common/models/modules/socket')(app.io,app);
  }	
});
