# FROM strongloop/node

# # You start off as the 'strongloop' user.
# # If a RUN command needs root, you can use sudo

# # In addition to standard Linux commands you also have access to node, npm,
# # and slc commands

# # It is common to copy your current
# ADD package.json /home/strongloop/app/package.json
# WORKDIR /home/strongloop/app
# RUN sudo npm install
# ADD . /home/strongloop/app
# #ENV DEBUG *
# EXPOSE  3000
# CMD [ "node", "."]

FROM mhart/alpine-node:7.7.1
MAINTAINER Keymetrics <contact@keymetrics.io>
#RUN npm install pm2 -g
#RUN npm install -g yarn
ADD package.json /home/app/package.json
#ADD yarn.lock /home/app/yarn.lock
WORKDIR /home/app
#RUN yarn install
ADD . /home/app
#ENV NODE_ENV qa1
EXPOSE  3000
CMD ["node", "."]